/**
 * \file
 *
 * \brief Functions to get measured values on ADC converter
 *
 *
 * \author pascal.sartoretti@hevs.ch
 *
 */

#include <htc.h>
#include <stdint.h>

#ifndef  ADC_H
#define ADC_H

/**
* \brief  Initialise the AD converter
* This function inits the ADC and selects the analog channels
* \param analogChannels Choose the analog channels
* \note A one on analogChannels means this channel is configured as analog
*/
void ADC_init(uint8_t analogChannels);

/**
* \brief Do an analog measure
* This function measures the selected analog channel
* \param channel Choose the analog channels to measure
* \return returns the ADC measured value
* \note This function waits until conversion completes
*/
uint16_t ADC_measure(uint8_t channel);

#endif
