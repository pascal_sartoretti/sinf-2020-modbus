/**
 * \file
 *
 * \brief Functions to communicate with the UART
 *
 *
 * \author pascal.sartoretti@hevs.ch
 *
 */#include <htc.h>
#include <stdint.h>

#ifndef  UART_H
#define UART_H

extern	uint8_t	rxBuffer[40];   ///< used to place received bytes
extern	uint8_t	txBuffer[40];   ///< used to prepare bytes to send

/**
* \brief  Initialise the UART interface
* This function inits the UART on PICEBS2 board
* \note Initialisation is made with 9600 bauds, 8 data bits, 2 stop bits
* \attention The CPU frequency has to be 8 MHz
*/
void UART_init(void);

/**
* \brief  Sent one byte on the UART
* This function just sent one byte on the UART interface and waits until sent
* \param data Byte to be sent on the serial line
* \note This function is blocking until byte has been sent
*/
void UART_write(uint8_t data);

/**
* \brief  Reads one byte from the UART
* This function returns one byte reads from the UART
* \return The byte reads from UART interface
* \note This function has to be called in interrupt
*/
uint8_t UART_read(void);

#endif
