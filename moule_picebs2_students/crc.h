/**
 * \file
 *
 * \brief CRC function
 *
 *
 * \author pascal.sartoretti@hevs.ch
 *
 */#include <pic18.h>
#include <stdint.h>

/**
* \brief  Calculate the CRC16
* This function calculates and returns the CRC16 of the given parameter
* \param puchMsg Message on wich to calculate the CRC16
* \param usDataLen Length of the message to calculate
*/
uint16_t CRC16( uint8_t * puchMsg, uint16_t usDataLen );


