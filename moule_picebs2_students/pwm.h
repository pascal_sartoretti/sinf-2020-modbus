/**
 * \file
 *
 * \brief Functions to works with PWM
 *
 *
 * \author pascal.sartoretti@hevs.ch
 */
#include <htc.h>
#include <stdint.h>

#ifndef  PWM_H
#define PWM_H

/**
* \brief  Initialise the PWM interface
* This function inits the PWM interface
* \note Initialisation is made for 10 bits duty cycle accuracy
*/
void PWM_init(void);

/**
* \brief  Initialise the PWM interface
* This function inits the PWM interface
* \param duty The duty cycle to obtain (0-1023)
*/
void PWM_set(uint16_t duty);

#endif
