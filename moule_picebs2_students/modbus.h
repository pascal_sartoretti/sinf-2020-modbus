/**
 * \file
 *
 * \brief Functions to works with modbus (over serial line)
 *
 *
 * \author pascal.sartoretti@hevs.ch
 *
 */
#include <htc.h>
#include <stdint.h>
#include "uart.h"
#include "crc.h"
#ifndef  MODBUS_H
#define MODBUS_H

extern uint16_t inputRegisters[10];		///< table of input registers
extern uint16_t holdingRegisters[10];	///< table of holding registers
uint8_t address;                        ///< address of modbus server

/**
* \brief  Initialise the Modbus
* This function inits the modbus interface
* \param modbusAddress The assigned modbus server address
*/
void MODBUS_init(uint8_t modbudAddress);

/**
* \brief  Send a modbus frame for answer
* This function sends a modbus frame
* \param msgPtr Pointer to message
* \param size Size of the message to send
*/
void MODBUS_send(uint8_t * msgPtr, uint8_t size);

/**
* \brief  Analyse a modbus frame for answer
* This function analyse a received frame
* \return A return code error could be usefull
* \note This function has to be called when a full frame has been received
*/
uint8_t MODBUS_Analyse(void);

#endif
